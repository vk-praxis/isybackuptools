function Backup-Databases {
param (
        [parameter(Mandatory=$true,Position=1)][xml]$xmlplan
        )
    if (!$script:prerequ -or ($script:prerequ.Status -contains "fail")){
        Stop-Execution
    }
    
    # Common resources
    $stepcnt = 0
    $script:xmlplan = $xmlplan
    $script:plan = $xmlplan.Plan
    $script:timestamp_start = Get-Date -format "dd-MM-yyyy-HH-mm-ss"
    $script:timestamp_start2 = Get-Date -format "dd.MM.yyyy HH:mm:ss"
    $script:timestamp_start3 = Get-Date -format "ddMMyy"

    try{
        New-Item -ItemType directory -Path $script:plan.Destination.TempPath -ErrorAction Stop | Out-Null
    } catch [System.IO.IOException] {
        Write-Warning "Problem creating directory '$($script:plan.Destination.TempPath)'. Details: $($_)"
    } catch { # catch [System.Management.Automation.DriveNotFoundException]
        Write-Error "Fatal error creating directory '$($script:plan.Destination.TempPath)'. Details: $($_)"
        Stop-Execution
    }

    [string]$logfolderpath = [io.path]::combine($script:plan.Destination.TempPath,"log$($script:timestamp_start)")
    try{
        New-Item -ItemType directory -Path $logfolderpath -ErrorAction Stop | Out-Null
    } catch {
        Write-Error "Fatal error creating directory  $(logfolderpath) '. Details: $($_)"
        Stop-Execution
    }

    # Initialize Variables
    $exludeddbs = $script:plan.Source.ExcludedDBs -split ","
    $step = "$([string]$stepcnt++) Do some pre-checks"

    $instancethere = $false
    $allinstances = Find-DbaInstance -ComputerName (Get-DbaComputerSystem).ComputerName
    foreach($instance in $allinstances){
    
        if ($script:plan.Source.Connection.Instance -eq "$($instance.ComputerName)\$($instance.InstanceName)"){
            $instancethere = $true
        }
    }
    if (!$instancethere){
        Write-Error "Instance $($script:plan.Source.Connection.Instance) not found. Found: $($allinstances | Out-String)"
        Append-Report $step "Instance $($script:plan.Source.Connection.Instance) not found. Found: $($allinstances | ConvertTo-Html -Property ComputerName,InstanceName -Fragment)" "Verify instance available." -fail
        Stop-Execution
    }
    try{
        $script:server = Connect-DbaInstance -SqlInstance $script:plan.Source.Connection.Instance
    } catch{ [System.Exception]
        $errorout = $_
        Append-Report $step $errorout $substep -fail
        Stop-Execution
    }

    Stop-DbaProcess -SqlInstance $script:server -Program "dbatools PowerShell module - dbatools.io" # Clear leftovers ??
    $databases_dbcc = $script:server.Databases.Where({($_.name -notlike "tempdb")})
    $databases = $databases_dbcc.Where({($exludeddbs -notcontains $_.Name)})

    # +++++++++++++++++++++++++++++++++++++
    # Do all necessary Backup-Steps
    # +++++++++++++++++++++++++++++++++++++
   
    # -------------------------------------
    # Step active database-connections
    # -------------------------------------
    
    $step = "$([string]$stepcnt++) Check active database-connections"
    $connectioncnt = 0
    $connectioncntdb = 0
    $str = ""
    foreach($db in $databases_dbcc) {
        $connectioncntdb = $db.ActiveConnections
        if ($db.IsSystemObject -eq $false){
            $connectioncnt = $connectioncnt + $connectioncntdb
        }
        $str = $str + "$($db.Name): $($connectioncntdb);"
    }
    if ($connectioncnt -gt 0){
        if ([System.Convert]::ToBoolean($script:plan.AbortOnActiveSessions)){
            Append-Report $step "Found: $($connectioncnt) active connections. $($str)" "Count-ActiveDBConnections" -fail
            Stop-Execution
        } else {
            Append-Report $step "Found: $($connectioncnt) active connections. $($str)" "Count-ActiveDBConnections" -warn
        }
    } else {
        Append-Report $step ("Found: 0 active connections on non-system-dbs. Datails: " + $str) "Count-ActiveDBConnections" -pass
    }
    # -------------------------------------
    # Step MS-DB-Consistency
    # -------------------------------------
    $step = "$([string]$stepcnt++) Check MS-DB-consistency"
    if ([System.Convert]::ToBoolean($script:plan.Validation.TestLastProductionIntegrityCheck)) { # TODO Excluded DBs
        $substep = "Retrieve last DBCC CHECKDB"
        $Args = @{Databases = $($databases_dbcc.Name -join ",").split(',')}
        $results = Get-DbaLastGoodCheckDb -SqlInstance $script:server -Verbose @Args
        #$results
        Append-Report $step "$($results | ConvertTo-Html -Property Database,LastGoodCheckDb -Fragment ) " $substep  -info
    }
    
    $substep = "DBCC CHECKDB"
    if ([System.Convert]::ToBoolean($script:plan.Validation.DoIntegrityCheck)) {
        try{
            foreach ($db in $databases_dbcc){
                [string]$dbcclogfilepath = [io.path]::combine($logfolderpath, "dbcc-checkdb-$($db.Name).html")
                [string]$dbccquery = "DBCC CHECKDB($($db)) WITH ALL_ERRORMSGS, TABLERESULTS;"
                $results = Invoke-Sqlcmd -ServerInstance $script:server -Query $dbccquery -QueryTimeout 3600 -ErrorAction Stop -Verbose #2>&1 4>&1 | Out-File $dbcclogfilepath
                $results | ConvertTo-Html | Out-File $dbcclogfilepath
                $criticalresults = $results.Where({$_.Level -gt 10})
                if (($criticalresults | Measure).Count -gt 0){
                    Append-Report $step "DBCC CHECKDB on $($db.Name) failed. Details: $($criticalresults| ConvertTo-Html -Property Level,Error,MessageText,RepairLevel -Fragment)" $substep  -fail -attachment $dbcclogfilepath
                    Stop-Execution
                } else {
                    Append-Report $step "DBCC CHECKDB on $($db.Name) executed successfully." $substep  -pass -attachment $dbcclogfilepath
                }                
            }
        } catch { 
            $errorout = $_
            Write-Error "Fatal error running 'DBCC CHECKDB'. Details: $($errorout)"
            Append-Report $step $errorout $substep -fail
            Stop-Execution
        }
    } else {
        Append-Report $step "DBCC CHECKDB disabled." $substep -proposal "Set Parameter Validation.DoIntegrityCheck in backup-plan to True." -warn
    }
    if ([System.Convert]::ToBoolean($script:plan.ValidationOnly)){
        Stop-Execution
    }

    # -------------------------------------
    # Step Create Backup
    # -------------------------------------
    $step = "$([string]$stepcnt++) Create backup of MS-DB on localhost"
    $substep = "Do Backup"
    $backupinfo = $null
    [string]$backupdirpath = [io.path]::combine($script:plan.Destination.TempPath,"databases")
    try{
        $backupinfo = Backup-DbaDatabase -SqlInstance $script:server -Databases $databases.Name -BackupDirectory $backupdirpath -Type Database -CreateFolder -Verbose
        Append-Report $step "Details: $($backupinfo | ConvertTo-Html -Property SqlInstance,DatabaseName,BackupComplete,BackupFilesCount,BackupFile,BackupFolder,Script -Fragment)" $substep -pass
    } catch{ [System.Exception]
	    $errorout = $_
        Append-Report $step $errorout $substep -fail
    }
    
    # -------------------------------------
    # Copy winacs-folder to TempPath
    # -------------------------------------
    $step = "$([string]$stepcnt++) Copy winacs-folder to TempPath"
    [string]$localcopypath=[io.path]::combine($script:plan.Destination.TempPath, "WINACS")
    $substep = "Copy with robocopy to $($localcopypath)."
    $results = Copy-WithProgress -Source $script:plan.Source.Filesystems.Path -Destination $localcopypath -ExcludedDirs DATA -LogFolderPath $logfolderpath
    Append-Report $step "Details: $($results | ConvertTo-Html -Fragment)" $substep  -fail:$(!($results.Status)) -pass:$($results.Status)
    if (!($results.Status)){
        Stop-Execution
    }

    # -------------------------------------
    # 2 Steps Create/append archive
    # -------------------------------------

    $compressedbackupfilename = "backup-isynet-$($script:timestamp_start).7z"
    $compressedbackupfilenamepath = [io.path]::combine($script:plan.Destination.TempPath,$compressedbackupfilename)
    $passw = $script:plan.Encryption.Password

    $step = "$([string]$stepcnt++) Compression DB-backup-files"
    $substep = "Compress Database and add to archive"
    try{
        foreach ($backupresult in $backupinfo){
            $7zipcmdargs = "a -t7z -m0=lzma2:d1024m -mx=9 -mfb=64 -md=32m -ms=on -mhe -p$($passw) $($compressedbackupfilename) ""$($backupresult.BackupPath)"""
            $result = Execute-Command -commandTitle "7zip.exe $($7zipcmdargs)" -commandPath $basicSettings.SevenZip -commandArguments $7zipcmdargs -workingDirectory $script:plan.Destination.TempPath
            $errormsgdetails = "Details: $($result | ConvertTo-Html -Property commandTitle,stdout,stderr,ExitCode -Fragment)"
            if([System.Convert]::ToBoolean($script:plan.ExcludePasswortInEmail)){
                $errormsgdetails = $errormsgdetails -replace "$($passw)","PW-HIDDEN-FOR-SECURITY-REASONS"
            }
            if ($result.ExitCode -ge 2){
                Append-Report $step "Error during compression of local filesystems. $($errormsgdetails)" $substep -fail
                Stop-Execution
            } else {
                Append-Report $step "Appending compressed database successfully to archive. $($errormsgdetails)" $substep -pass
            }
        }
    } catch {
	    $errorout = $_
        Append-Report $step $errorout $substep -fail
        Stop-Execution
    }

    $step = "$([string]$stepcnt++) Compression isynet local filesystems"
    $substep = "Create .7z's of specified local filesystems."
    try{
        #TODO: allow multiple sources paths
        $7zipcmdargs = "a -t7z -m0=lzma2:d1024m -mx=9 -mfb=64 -md=32m -ms=on -mhe -p$($passw) $($compressedbackupfilename) ""$($localcopypath)"""
        $result = Execute-Command -commandTitle "7zip.exe $($7zipcmdargs)" -commandPath $basicSettings.SevenZip -commandArguments $7zipcmdargs -workingDirectory $script:plan.Destination.TempPath
        $errormsgdetails = "Details: $($result | ConvertTo-Html -Property commandTitle,stdout,stderr,ExitCode -Fragment)"
        if([System.Convert]::ToBoolean($script:plan.ExcludePasswortInEmail)){
            $errormsgdetails = $errormsgdetails -replace "$($passw)","PW-HIDDEN-FOR-SECURITY-REASONS"
        }
        if ($result.ExitCode -ge 2){
            Append-Report $step "Error during compression of local filesystems. $($errormsgdetails)" $substep -fail
            Stop-Execution
        } else {
            Append-Report $step "Compression of local filesystems successful. $($errormsgdetails)" $substep -pass
        }
    } catch {
	    $errorout = $_
        Append-Report $step $errorout $substep -fail
        Stop-Execution
    }
    
    # -------------------------------------
    # Backup Retention and Cleanup
    # -------------------------------------    
    $step = "$([string]$stepcnt++) Backup Retention and Cleanup"
    $substep = "Remove old database full backups."
    Remove-DbaBackup -BackupFolder $backupdirpath -BackupFileExtension bak -RetentionPeriod 48h
    # Remove old zip-files
    $substep = "Remove old backups."

    CleanOldBackups -toclean $script:plan.Destination.TempPath -prefix "backup-isynet-" -suffix ".7z" -keepversions 2 -Verbose

    CleanOldBackups -toclean $script:plan.Destination.TempPath -prefix "log" -suffix "" -keepversions 2 -Verbose
    
    Append-Report $step "Deleted old compressed backups and log files. (Keep last month, previous monday, day before yesterday, yesterday and keep respectively max. 2 versions)." $substep -pass
        
    # -------------------------------------
    # Copy 7zips and logs to remote
    # -------------------------------------
    $step = "$([string]$stepcnt++) Copy 7zips and logs to remote"
    $substep = "Copy with robocopy to $($script:plan.Destination.RemotePath)."

    $result = cmd /c "2>&1" net use $script:plan.Destination.RemotePath | Out-String
    if (!($LASTEXITCODE -eq 0)) {
        Append-Report $step "Problem connecting '$($script:plan.Destination.RemotePath)'. Details: $($result)" $substep -fail
        Stop-Execution
    }

    
    if ([bool]([System.Uri]$script:plan.Destination.RemotePath).IsUnc){ # (Test-Path -Path $script:plan.Destination.RemotePath -IsValid) -and 
        $results = Copy-WithProgress -Source $script:plan.Destination.TempPath -Destination $script:plan.Destination.RemotePath -ExcludedDirs WINACS,databases -LogFolderPath $logfolderpath
        Append-Report $step "Details: $($results | ConvertTo-Html -Fragment)" $substep -pass
    } else {
        Append-Report $step "'$($script:plan.Destination.RemotePath)' not a valid path." $substep -fail
    }

    Stop-Execution  

}
