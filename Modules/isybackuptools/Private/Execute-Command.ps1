﻿function Execute-Command {
param (
        [parameter(Mandatory=$true,Position=1)][string]$commandTitle,
        [parameter(Mandatory=$true,Position=2)][string]$commandPath,
        [parameter(Mandatory=$false,Position=3)][string]$commandArguments="",
        [parameter(Mandatory=$false,Position=4)][string]$workingDirectory=""
        )

  Try {
    $pinfo = New-Object System.Diagnostics.ProcessStartInfo
    $pinfo.CreateNoWindow = $true
    $pinfo.FileName = $commandPath
    $pinfo.RedirectStandardError = $true
    $pinfo.RedirectStandardOutput = $true
    $pinfo.UseShellExecute = $false
    $pinfo.Arguments = $commandArguments
    if ($workingDirectory){
        $pinfo.WorkingDirectory = $workingDirectory
    }
    $p = New-Object System.Diagnostics.Process
    $p.StartInfo = $pinfo
    $p.Start() | Out-Null
    $obj = [pscustomobject]@{
        commandTitle = $commandTitle
        stdout = $p.StandardOutput.ReadToEnd()
        stderr = $p.StandardError.ReadToEnd()
        ExitCode = $p.ExitCode  
    }
    $p.WaitForExit()
    return $obj
  }
  Catch { [System.Exception]
    $errorout = $_
    return [pscustomobject]@{
        commandTitle = $commandTitle
        stdout = ""
        stderr = $errorout
        ExitCode = 10
    }
  }
}