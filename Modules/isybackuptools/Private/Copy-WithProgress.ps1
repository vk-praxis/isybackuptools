﻿function Copy-WithProgress {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)][string] $Source
        , [Parameter(Mandatory = $true)][string] $Destination
        , [int] $Gap = 0
        , [int] $ReportGap = 2000
        , [array] $ExcludedDirs = @()
        , [string] $LogFolderPath = ""
    )
    if (!$LogFolderPath){
        $LogFolderPath = "$($env:windir)\temp"
    }
    $RobocopyLogPath = [io.path]::combine($LogFolderPath,"robocopy $(Get-Date -Format 'yyyy-MM-dd hh-mm-ss').log")
    $StagingLogPath = [io.path]::combine($LogFolderPath,"robocopy_staging $(Get-Date -Format 'yyyy-MM-dd hh-mm-ss').log")
    # Define regular expression that will gather number of bytes copied
    $RegexBytes = '(?<=\s+)\d+(?=\s+)';

    #region Robocopy params
    # MIR = Mirror mode
    # NP  = Don't show progress percentage in log
    # NC  = Don't log file classes (existing, new file, etc.)
    # BYTES = Show file sizes in bytes
    # NJH = Do not display robocopy job header (JH)
    # NJS = Do not display robocopy job summary (JS)
    # TEE = Display log in stdout AND in target log file
    # COPY:DT = NAS
    # r:<N> = amount retry
    # w:<N> = wait for N seconds to retry

    $CommonRobocopyParams = '/MIR /NP /NDL /NC /BYTES /NJH /NJS /COPY:DT /r:3 /w:5';

    $Excluded = ""
    if (($ExcludedDirs | Measure).Count -gt 0){
        $Excluded = "/XD $($ExcludedDirs -join " ")"
    }
    $CommonRobocopyParams = "$($CommonRobocopyParams) $($Excluded)"
    #endregion Robocopy params

    #region Robocopy Staging
    Write-Verbose -Message 'Analyzing robocopy job ...';
    #$StagingLogPath = '{0}\temp\{1} robocopy staging.log' -f $env:windir, (Get-Date -Format 'yyyy-MM-dd hh-mm-ss');

    $StagingArgumentList = '"{0}" "{1}" /LOG:"{2}" /L {3}' -f $Source, $Destination, $StagingLogPath, $CommonRobocopyParams;
    Write-Verbose -Message ('Staging arguments: {0}' -f $StagingArgumentList);
    Start-Process -Wait -FilePath robocopy.exe -ArgumentList $StagingArgumentList -NoNewWindow;
    # Get the total number of files that will be copied
    $StagingContent = Get-Content -Path $StagingLogPath;
    $TotalFileCount = $StagingContent.Count - 1;

    # Get the total number of bytes to be copied
    [RegEx]::Matches(($StagingContent -join "`n"), $RegexBytes) | % { $BytesTotal = 0; } { $BytesTotal += $_.Value; };
    Write-Verbose -Message ('Total bytes to be copied: {0}' -f $BytesTotal);
    #endregion Robocopy Staging

    #region Start Robocopy
    # Begin the robocopy process
    $ArgumentList = '"{0}" "{1}" /LOG:"{2}" /ipg:{3} {4}' -f $Source, $Destination, $RobocopyLogPath, $Gap, $CommonRobocopyParams;
    Write-Verbose -Message ('Beginning the robocopy process with arguments: {0}' -f $ArgumentList);
    $Robocopy = Start-Process -FilePath robocopy.exe -ArgumentList $ArgumentList -PassThru -NoNewWindow -RedirectStandardOutput "$([io.path]::combine($LogFolderPath,"robocopy.out"))" -RedirectStandardError "$([io.path]::combine($LogFolderPath,"robocopy.errout"))"
    $handle = $Robocopy.Handle #resolves bug with Start-Process without -wait --> empty fields for exitcode and exittime
    Start-Sleep -Milliseconds 100;
    #endregion Start Robocopy

    #region Progress bar loop
    while (!$Robocopy.HasExited) {
        Start-Sleep -Milliseconds $ReportGap;
        $BytesCopied = 0;
        $LogContent = Get-Content -Path $RobocopyLogPath;
        $BytesCopied = [Regex]::Matches($LogContent, $RegexBytes) | ForEach-Object -Process { $BytesCopied += $_.Value; } -End { $BytesCopied; };
        $CopiedFileCount = $LogContent.Count - 1;
        Write-Verbose -Message ('Bytes copied: {0}' -f $BytesCopied);
        Write-Verbose -Message ('Files copied: {0}' -f $LogContent.Count);
        $Percentage = 0;
        if ($BytesCopied -gt 0) {
           $Percentage = (($BytesCopied/$BytesTotal)*100)
        }
        Write-Progress -Activity Robocopy -Status ("Copied {0} of {1} files; Copied {2} of {3} bytes" -f $CopiedFileCount, $TotalFileCount, $BytesCopied, $BytesTotal) -PercentComplete $Percentage
    }
    #endregion Progress loop

    $exitmessage = switch ($Robocopy.ExitCode){
        0 {"No files were copied. No failure was encountered. No files were mismatched. The files already exist in the destination directory; therefore, the copy operation was skipped."}
        1 {"All files were copied successfully."}
        2 {"There are some additional files in the destination directory that are not present in the source directory. No files were copied."}
        3 {"Some files were copied. Additional files were present. No failure was encountered."}
        4 {"Some Mismatched files or directories were detected. Examine the output log. Housekeeping might be required."}
        5 {"Some files were copied. Some files were mismatched. No failure was encountered."}
        6 {"Additional files and mismatched files exist. No files were copied and no failures were encountered. This means that the files already exist in the destination directory."}
        7 {"Files were copied, a file mismatch was present, and additional files were present."}
        8 {"Several files did not copy."}
        16 {"Serious error. Robocopy did not copy any files. Either a usage error or an error due to insufficient access privileges on the source or destination directories."}
        default {"Unknown error Message"}
    }
    $status = $true
    if ($Robocopy.ExitCode -gt 7){
        $status = $false
    }
    
   # $ec = $Robocopy.GetType().GetField("exitCode", "NonPublic,Instance").GetValue($Robocopy) 
  #  $et = $Robocopy.GetType().GetField("exitTime", "NonPublic,Instance").GetValue($Robocopy) 
    #region Function output
    [PSCustomObject]@{
        BytesCopied = $BytesCopied;
        FilesCopied = $CopiedFileCount;
        ExitCode = $Robocopy.ExitCode;
        ExitMessage = $exitmessage
        Status = $status
        StartTime = $Robocopy.StartTime;
        ExitTime = $Robocopy.ExitTime;
    };
    #endregion Function output
}