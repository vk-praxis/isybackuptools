﻿function IsWithinLastDays {
param (
    [parameter(Mandatory=$true,Position=1)][datetime]$when,
    [parameter(Mandatory=$false,Position=2)][int]$from= 1,
    [parameter(Mandatory=$false,Position=3)][int]$to = 0
    )
    $date = [datetime](Get-Date)
    return (($when.AddDays($from) -gt $date) -and ($when.AddDays($to) -le $date))
}

function IsOnLastWeekDay {
param (
    [parameter(Mandatory=$true,Position=1)][datetime]$when,
    [parameter(Mandatory=$false,Position=2)][string]$weekday="Monday"
    )
    $date = Get-Date
    for($i=1; $i -le 7; $i++)
    {        
        if($date.AddDays(-$i).DayOfWeek -eq $weekday)
        {
            $date = $date.AddDays(-$i)
            break
        }
    }
    
    return ($date.Date -eq $when.Date)
}

function IsOnLastMonthDay {
param (
    [parameter(Mandatory=$true,Position=1)][datetime]$when,
    [parameter(Mandatory=$false,Position=2)][int]$monthday=1
    )
    $date = Get-Date
    for($i=1; $i -le 31; $i++)
    {        
        if($date.AddDays(-$i).Day -eq $monthday)
        {
            $date = $date.AddDays(-$i)
            break
        }
    }
    return ($date.Date -eq $when.Date)
}

function IsOnLastQuartalStart {
param (
    [parameter(Mandatory=$true,Position=1)][datetime]$when,
    [parameter(Mandatory=$false,Position=2)][int]$monthday=1
    )
    $date = Get-Date
    for($i=1; $i -le 93; $i++)
    {        
        if(($date.AddDays(-$i).Day -eq $monthday) -and (($date.AddDays(-$i).Month % 3 ) -eq 1))
        {
            $date = $date.AddDays(-$i)
            $date
            break
        }
    }
    return ($date.Date -eq $when.Date)
}

function IsOldBackup {
param (
    [parameter(Mandatory=$true,Position=1)][datetime]$when
    )
    $date = Get-Date
    return (-not ((IsWithinLastDays -from 1 -to 0 -when $when) -or (IsWithinLastDays -from 2 -to 1 -when $when) -or (IsOnLastWeekDay -when $when) -or (IsOnLastMonthDay -when $when))) # or (IsOnLastQuartalStart -when $when)
}

function CutString {
param (
    [parameter(Mandatory=$true,Position=1)][string]$tocut,
    [parameter(Mandatory=$true,Position=2)][AllowEmptyString()][string]$prefix,
    [parameter(Mandatory=$true,Position=3)][AllowEmptyString()][string]$suffix
    )
    return (($tocut -replace "^$($prefix)","") -replace "$($suffix)$","" )
}

#TODO: Retention days for loop; sorting newest versions
function CleanOldBackups {
param (
    [parameter(Mandatory=$true,Position=1)][string]$toclean,
    [parameter(Mandatory=$false)][string]$prefix = "",
    [parameter(Mandatory=$false)][string]$suffix = "",
    [parameter(Mandatory=$false)][int]$keepversions=1
    )
    # Delete all files not in range
    Get-ChildItem -Path $toclean -Filter "$($prefix)*$($suffix)" -Force | Where-Object { IsOldBackup -when ([System.DateTime]::ParseExact((CutString $_.BaseName $prefix $suffix), "dd-MM-yyyy-HH-mm-ss", $null))} | Remove-Item -Force -Recurse -Verbose
    # Select all remaining files again
    $files = {Get-ChildItem -Path $toclean -Filter "$($prefix)*$($suffix)" -Force}.Invoke()
    # Delete Files in last 24 hours; keep $keepversions versions
    $keepgroup = $files | Where-Object { IsWithinLastDays -from 1 -to 0 -when ([System.DateTime]::ParseExact((CutString $_.BaseName $prefix $suffix), "dd-MM-yyyy-HH-mm-ss", $null))} | Sort-Object -Property LastWriteTime -Descending
    $i = 0
    foreach ($element in $keepgroup) {
        $i++
        if ($i -gt $keepversions){
            $files.Remove($element)
            $element | Remove-Item -Force -Recurse -Verbose
        }
    }
    # Delete Files in last 48 - 24 hours; keep $keepversions versions
    $keepgroup = $files | Where-Object { IsWithinLastDays -from 2 -to 1 -when ([System.DateTime]::ParseExact((CutString $_.BaseName $prefix $suffix), "dd-MM-yyyy-HH-mm-ss", $null))} | Sort-Object -Property LastWriteTime -Descending
    $i = 0
    foreach ($element in $keepgroup) {
        $i++
        if ($i -gt $keepversions){
            $files.Remove($element)
            $element | Remove-Item -Force -Recurse -Verbose
        }
    }
    $keepgroup = $files | Where-Object { IsOnLastWeekDay -when ([System.DateTime]::ParseExact((CutString $_.BaseName $prefix $suffix), "dd-MM-yyyy-HH-mm-ss", $null))} | Sort-Object -Property LastWriteTime -Descending
    $i = 0
    foreach ($element in $keepgroup) {
        $i++
        if ($i -gt $keepversions){
            $files.Remove($element)
            $element | Remove-Item -Force -Recurse -Verbose
        }
    }
    $keepgroup = $files | Where-Object { IsOnLastMonthDay -when ([System.DateTime]::ParseExact((CutString $_.BaseName $prefix $suffix), "dd-MM-yyyy-HH-mm-ss", $null))} | Sort-Object -Property LastWriteTime -Descending
    $i = 0
    foreach ($element in $keepgroup) {
        $i++
        if ($i -gt $keepversions){
            $files.Remove($element)
            $element | Remove-Item -Force -Recurse -Verbose
        }
    }


}