﻿function Append-Report{
    param(
        [parameter(Mandatory=$true,Position=1)][string]$step,
        [parameter(Mandatory=$true,Position=2)][string]$message,
        [parameter(Mandatory=$false,Position=3)][string]$subStep,
        [parameter(Mandatory=$false,Position=4)][string]$proposal,
        [parameter(Mandatory=$false)][string]$attachment="",
        [parameter(Mandatory=$false)][switch]$fail,
        [parameter(Mandatory=$false)][switch]$warn,
        [parameter(Mandatory=$false)][switch]$info,
        [parameter(Mandatory=$false)][switch]$pass
    )

    if ($fail){
        $status = "fail"
    }elseif($warn){
        $status = "warn"
    } elseif($info) {
        $status = "info"
    } else {
        $status = "pass"
    }
    $finished = Get-Date -format "dd-MM-yyyy HH:mm:ss"
    $newobj = @{SubStep=$subStep;Message=$message;Status=$status; Attachment=$attachment; Proposal=$proposal; Finished=$finished;}
    #$newobj
    if (!$script:report.contains($step)){
        $script:report[$step] = @()
    }
    $script:report[$step] += $newobj

    if ($fail){
       # Stop-Execution
    }
}

function Perform-BackupReport{
### Begin report generation

    $status = "pass"
    $attachments = @()
	#Create HTML Report
	#Common HTML head and styles
	$htmlhead="<html>
				<style>
				BODY{font-family: Arial; font-size: 8pt; }
				H1{font-size: 16px;}
				H2{font-size: 14px;}
				H3{font-size: 12px;}
				TABLE{border: 1px solid black; border-collapse: collapse; font-size: 8pt; }
				TH{border: 1px solid black; background: #dddddd; padding: 5px; color: #000000; max-width:200px; }
				TD{border: 1px solid black; padding: 5px; max-width:200px; }
				td.pass{background: #7FFF00;}
				td.warn{background: #FFE600;}
				td.fail{background: #FF0000; color: #ffffff;}
				td.info{background: #85D4FF;}
				</style>
				<body>
				<h1 align=""center"">Isybackuptools report - DB & custom filesystem-locations</h1>
				<h3 align=""center"">Started at: $($script:timestamp_start2)</h3>"
    $sqlhtmltablecontent1 = "<tr><td colspan='2'><h3>Step: Prerequisites</h3></td></tr><tr>
						<th>Substep</th>
						<th>Message</th>
						</tr>"

    foreach($substep in $script:prerequ){
        $sqlhtmltablecontent1 += "<tr><td class='$($substep.Status)'>$($substep.SubStep)</td><td class='$($substep.Status)'>$($substep.Message)</td></tr>"
        if($substep.Status -eq "fail"){
            $status = "fail"
        }
    }
    $sqlhtmltablecontent1 += "<tr><td colspan='2'>&#x2714;&#x2718;</td></tr>"
    $sqlhtmltablecontent2 = ""
    #$script:report
    foreach($step in $script:report.GetEnumerator() | Sort Name){
        #$step
        $sqlhtmltablecontent2 += "<tr><td colspan='2'><h3>Step: $($step.Name)</h3></td></tr>"
        foreach($substep in $step.Value){
            $sqlhtmltablecontent2 += "<tr><th width='8%'>Substep</th><td width='92%' class='$($substep.Status)'>$($substep.SubStep)</td></tr>
                                      <tr><th width='8%'>Message</th><td width='92%' class='$($substep.Status)'>$($substep.Message)</td></tr>
                                      <tr><th width='8%'>Attachment</th><td width='92%' class='$($substep.Status)'>$($substep.Attachment)</td></tr>
                                      <tr><th width='8%'>Finished</th><td width='92%' class='$($substep.Status)'>$($substep.Finished)</td></tr>
                                      <tr><td colspan='2'></td></tr>"
            if ($substep.Attachment){
                $attachments += $substep.Attachment
            }
            if($substep.Status -eq "fail"){
                $status = "fail"
            }
        }
        $sqlhtmltablecontent2 += "<tr><td colspan='2'>&#x2714;&#x2718;</td></tr>"
    }
	#Exchange Server Health Report Table Header
	$sqlhtmltable = "<h3>Task parameters</h3>
						<p>See attachment plan.xml</p>
                     <h3>powershell prerequesites</h3>
						<p></p>
						<table width='100%'>
                        $($sqlhtmltablecontent1)
						</table>
                     <h3>progress report</h3>
						<p></p>
						<table width='100%'>
						$($sqlhtmltablecontent2)</table>"
	

	$htmltail = "</body>
				</html>"

	$htmlreport = $htmlhead + $sqlhtmltable + $htmltail
	
	#if ($ReportMode -or $ReportFile)
	#{
	#	$htmlreport | Out-File $ReportFile -Encoding UTF8
	#}

    $subject = "[isybackuptools] report - $($script:timestamp_start2) - status:$($status)"

    $emailSettings = $basicSettings.Logging.Email
    $smtpSettings = $emailSettings.SMTP

	if ([System.Convert]::ToBoolean($emailSettings.Send)){
            
        [string]$xmplanpath = [io.path]::combine($script:plan.Destination.TempPath,"log$($script:timestamp_start)","plan.xml")

        if([System.Convert]::ToBoolean($script:plan.ExcludePasswortInEmail)){
            $script:plan.Encryption.Password = "PW-HIDDEN-FOR-SECURITY-REASONS"
        }

        try {
            $script:xmlplan.Save($xmplanpath)
            $attachments += $xmplanpath
        } catch {
            Write-Warning "XML-Plan konnte nicht abgespeichert werden."
        }
            
			
        #Send email message
        $password = "$($smtpSettings.Password)" | Convertto-SecureString -AsPlainText -Force
        $creds = New-Object System.Management.Automation.Pscredential($smtpSettings.Login,$password)
        $props = @{
            From = $emailSettings.From 
            To= $emailSettings.To 
            Subject = $subject 
            Body = $htmlreport 
            SmtpServer = $smtpSettings.Server 
            Port = $smtpSettings.Port
            Credential = $creds
            Encoding = ([System.Text.Encoding]::UTF8)
            BodyAsHtml = $true
        }

        if($emailSettings.CC){
            $props.Add("CC",$emailSettings.CC)
        }
        if($attachments){
            $props.Add("Attachments",$attachments)
        }
        try{
            Send-MailMessage @props
        } catch{
            $errorout = $_
            Write-Warning $errorout
        }
	}
    try {
        $htmlreport | Out-File "$($script:plan.Destination.TempPath)\log$($script:timestamp_start)\Report.html" -ErrorAction Stop
    } catch {
        $errorout = $_
        Write-Warning $errorout
    }
	
}
function Stop-Execution{

    # Perform, mail and save the report
    Perform-BackupReport

    # Cleanup try to disconnect
    try{
        Stop-DbaProcess -SqlInstance $script:server -Program "dbatools PowerShell module - dbatools.io"
        $script:server.ConnectionContext.ForceDisconnected()
    } catch {
        $errorout = $_
        Write-Warning $errorout
    }
    EXIT
}