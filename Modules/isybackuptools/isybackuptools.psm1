param(
    [parameter(Mandatory=$true)][xml]$configFile=$true
)
$prerequ = @()
$report = @{}

$server = $null;
$databases = $null;

$timestamp_start =$null;

$basicSettings = $configFile.Settings
$mode = ""

#Powershell version
$psok = "pass"
if($PSVersionTable.PSVersion.Major -lt 5){
    $psok = "fail"
    Write-Error "Please update powershell above or equal 5."    
} 
$prerequ+= @{
    SubStep = "Check powershell version"
    Message = "$($PSVersionTable.GetEnumerator()  | % { "$($_.Name)=$($_.Value)" })"
    Status = ($psok)
    Attachment = $null
}


#Get public and private function definition files.
$Public  = @( Get-ChildItem -Path $PSScriptRoot\Public\*.ps1 )
$Private = @( Get-ChildItem -Path $PSScriptRoot\Private\*.ps1 -ErrorAction SilentlyContinue )
#Dot source the files
    Foreach($import in @($Public + $Private))
    {
        Try
        {
            . $import.fullname
        }
        Catch
        {
            Write-Error -Message "Failed to import function $($import.fullname): $_"
        }
    }
# Import the required modules
$step = "LoadModules"
#Save current directory (required, because module "SQLPS" changes cwd)
Push-Location
#Import the SQL Server Module.
try {
    #Import-Module -Name SQLPS -DisableNameChecking -Force
    $prerequ+= @{SubStep="Load Module sqlserver";Message="";Status="pass"; Attachment=$null}
}
catch {	
    #Module was not loaded -> report and prevent further executions: $prerequisites = $false
    $emsg = "Problem while loading sqlserver-Module. Exception-Message: '$($_.Exception.Message)'. Please make sure, that it's installed with running: 'Import-Module -Name SQLPS -Verbose'"
    $prerequ+= @{SubStep="Load Module sqlserver";Message=$emsg;Status="fail"; Attachment=$null}
    Write-Error $emsg
}
#Restore previous location
Pop-Location

#Import the dbatools Powershell Module.
try {
    Import-Module -Name dbatools -DisableNameChecking -ErrorAction Stop
    $prerequ+= @{SubStep="Load Module dbatools";Message="";Status="pass"; Attachment=$null}
}
catch {	
    #Module was not loaded -> report and prevent further executions: $prerequisites = $false
    $emsg = "Problem while loading dbatools-Module. Exception-Message: '$($_.Exception.Message)'. Please make sure, that it's installed with running: 'Import-Module -Name dbatools -Verbose'"
    $prerequ+= @{SubStep="Load module dbatools";Message=$emsg;Status="fail"; Attachment=$null}
    Write-Error $emsg
}

if (!(Test-Path $basicSettings.SevenZip)){
    $prerequ+= @{SubStep="Verify 7zip installation";Message="Could not find 7zip in specified folder '$($basicSettings.SevenZip)'.";Status="fail"; Attachment=$null}
    Write-Error "Could not find 7zip in specified folder '$($basicSettings.SevenZip)'."
}

Export-ModuleMember -Function $Public.Basename