﻿$myDir = Split-Path -Parent $MyInvocation.MyCommand.Path

# Import global settings from config file
[xml]$ConfigFile = Get-Content "$MyDir\..\globalsettings.xml"

# Import isybackuptools with above config file
Import-Module isybackuptools -ArgumentList $ConfigFile -Verbose -Force

# Import backup plan for specific instance
[xml]$BackupPlan = Get-Content "$MyDir\plan.xml"

# Create a backup with the specified plan
Backup-Databases $BackupPlan