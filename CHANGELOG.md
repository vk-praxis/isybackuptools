**Note:** 

## 0.2.0 (2018-06-09)
- Released publicly
- Minor fixes (code cleanup)
- Added .7z archive encryption
- Removed disk burning
- Parameter cleanup
- Added instructions

## 0.1.0 (2017-05-10)
- All core functionalities 