# isybackuptools
Convenient backup solution for [x.isynet medical practice management software](https://arztsoftware.medatixx.de/kundenservice/xisynet/) hosted with Microsoft SQL Server (Express Edition).
x.isynet in combination with MSQLServer-EE stores some data in the database and some in the local filesystem. In order to make a consistent backup one has to backup both preferably at the same time. This script provides a robust, but not ideal solution for this task. Additionally it does constistency checks on the db, manages the backups and informs with an informative e-mail report.
## prerequisites
* powershell-version: >=5
* powershell-modules: [SqlServer](https://msdn.microsoft.com/de-de/library/hh231286.aspx), [dbatools](https://dbatools.io/download/) ([git-repo](https://github.com/sqlcollaborative/dbatools))
* installed programs: [7Zip](https://www.7-zip.org),[VC++ 2013 Redistributable (64 bit)](https://www.microsoft.com/de-de/download/details.aspx?id=40784)

## installation and configuration
### install prerequisites
1. Download and install [Windows Management Framework](https://www.microsoft.com/en-us/download/details.aspx?id=54616) for the latest powershell version.
2. Run `Install-Module dbatools` and `Install-Module sqlserver` in a x64 powershell with admin access and confirm twice each time.

### install isybackuptools
isybackuptools consists of a series of PowerShell scripts, a PowerShell module, and some XML configuration files. Installation is performed manually using the following steps.
1. Download the latest Zip file from [this repo]().
2. Extract or copy the following files and folders to your server. For example, place all of the files and folders in the `C:\Scripts\isybackuptools` folder.
	- `\Jobs`
	- `\Modules`
3. Unblock the `.psm` and `.psd` files. [(Read more)](https://blogs.msdn.microsoft.com/delay/p/unblockingdownloadedfile/).
4. Copy the subfolder of the Modules folder `isybackuptools` to `C:\Windows\System32\WindowsPowerShell\v1.0\Modules\`

### adapt module configuration and the backup plan to your purposes
1. Edit `C:\Scripts\isybackuptools\Jobs\globalsettings.xml` :
    - Adapt the email-settings inclusive the SMTP-settings for e-mail 
    - Adapt the 7.zip program path if not already installed in the specified location
2. Edit `C:\Scripts\isybackuptools\Jobs\Backup\Plan.xml`
    - Make sure you adapt the destinations correctly. Those folders are overridden with the backup data.
    - On Default validation is enabled. This means that CHECKDB will be performed on before the backup.
    - Make sure to set a encryption password and remember it.  

### create a windows task 
1. Run Task Scheduler
2. Click on "Create task ..." in  the sidebar
3. On "General" set a name, description, set "Run whether user is logged on or not" and "Run with highest priviliges"
4. On "Triggers" add a trigger.
5. On "Actions" add an action with:
    - with command: `cmd` 
    - and parameter: `/c C:\Windows\system32\WindowsPowerShell\v1.0\powershell.exe -NoLogo -NonInteractive -ExecutionPolicy Unrestricted -Command "& {C:\Scripts\isybackuptools\Jobs\Backup\job.ps1}" > C:\Scripts\isybackuptools\Jobs\Backup\lastlog.txt`
    - execute in: `C:\Scripts\isybackuptools\Jobs\Backup`

## license
The tool is licensed under [MIT](/LICENSE).

## todo
- Use something like freefilesync instead of robocopy to ensure no data gets corrupted during copy process. 
- Use filesystem snapshot to offer a consistent backup.
- Include restoring and test-restoring.

## faq
### has this tool some limitations?
- yes, it doesn't prevent you from silence data corruption in the filesystem. (In order to solve this permanently, you'll need a better filesystem (e.g. ZFS or ReFS)) But:
   - it will inform you about `DBCC CHECKDB` errors which indicates a faulty disk
   - if you use smartctl regulary you can predict such a failure
   - by using ecc-ram you can further minimize bit-flips, because most flips occur in ram not in hdd

### how do I restore my data?
currently restoring should be done manually according to the following instructions:
1. Copy the .7z locally and extract with the correct password via gui
2. Install x.isynet with db
3. Replace the `WINACS` folder with the extracted one 
4. Use the [SQL Server Management Studio (SSMS)](https://docs.microsoft.com/en-us/sql/powershell/download-sql-server-ps-module?view=sql-server-2017) to remove the winacs db
5. Import the db from your extracted backup

### what should I do regarding this error: 
"Die Datei oder Assembly "Microsoft.SqlServer.BatchParser.dll" oder eine Abhängigkeit davon wurde nicht gefunden."
1. install [VC++ 2013 Redistributable (64 bit)](https://www.microsoft.com/de-de/download/details.aspx?id=40784)
2. restart